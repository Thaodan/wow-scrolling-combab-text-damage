## Interface: 70300
## Title: SCTD
## Notes: Adds your damage to Scrolling Combat Text
## Notes-ruRU: Выводит ваш урон в Scrolling Combat Text
## Version: 7.3
## Author: Thaodan
## X-Credits: Grayhoof
## Dependencies: sct
## X-Website: https://gitlab.com/Thaodan/wow-scrolling-combab-text-damage
## X-Category: Combat
## X-RelSite-WoWI: 4913
## OptionalDeps:
sctd.lua
sctd.xml
locals\localization.lua
locals\localization.de.lua
locals\localization.ru.lua
locals\localization.fr.lua
locals\localization.es.lua
locals\localization.kr.lua
locals\localization.tw.lua
locals\localization.cn.lua


